/*
| Overlay of different styles of pass hashes.
*/
def.attributes =
{
	ldap: { type: [ 'undefined', 'PassHash/Ldap' ], json: true },

	plain: { type: [ 'undefined', 'PassHash/Plain' ], json: true },

	shadow: { type: [ 'undefined', 'PassHash/Shadow' ], json: true },
};

def.json = true;

import { Self as Ldap   } from '{PassHash/Ldap}';
import { Self as Plain  } from '{PassHash/Plain}';
import { Self as Shadow } from '{PassHash/Shadow}';

/*
| Checks a password.
|
| ~return true: password match
| ~return false: password mismatch
| ~return undefined: no passhasshes stored
*/
def.proto.checkPassword =
	function( password )
{
	let result;
	{
		const shadow = this.shadow;
		if( shadow )
		{
			result = false;
			if( shadow.checkPassword( password ) ) return true;
		}
	}
	{
		const ldap = this.ldap;
		if( ldap )
		{
			result = false;
			if( ldap.checkPassword( password ) ) return true;
		}
	}
	{
		const plain = this.plain;
		if( plain )
		{
			result = false;
			if( plain.checkPassword( password ) ) return true;
		}
	}
	return result;
};

/*
| An empty passhash overlay.
|
| Password checking aginst this will always be false.
*/
def.staticLazy.Empty =
	function( )
{
	return Self.create( );
};

/*
| Creates a passhash overlay with a ldap hash password.
*/
def.static.Ldap =
	function( password )
{
	const ldap = Ldap.Password( password );
	return Self.create( 'ldap', ldap );
};

/*
| Creates a passhash overlay with a ldap password set.
*/
def.proto.Ldap =
	function( password )
{
	const ldap = Ldap.Password( password );
	return this.create( 'ldap', ldap );
};

/*
| Creates a passhash overlay with a plain password.
*/
def.static.Plain =
	function( password )
{
	const plain = Plain.Password( password );
	return Self.create( 'plain', plain );
};

/*
| Creates a passhash overlay with a plain password set.
*/
def.proto.Plain =
	function( password )
{
	const plain = Plain.Password( password );
	return this.create( 'plain', plain );
};

/*
| Creates a passhash overlay with a shadow hash password.
*/
def.static.Shadow =
	function( password )
{
	const shadow = Shadow.Password( password );
	return Self.create( 'shadow', shadow );
};

/*
| Creates a passhash overlay with a shadow password set.
*/
def.proto.Shadow =
	function( password )
{
	const shadow = Shadow.Password( password );
	return this.create( 'shadow', shadow );
};

/*
| Creates a passhash overlay with all defined passwords changed.
| creates a shadow overlay nevertheless.
*/
def.proto.ChangeAll =
	function( password )
{
	let o = this;

	if( !o.ldap && !o.plain && !o.shadow ) throw new Error( 'no overlays to change' );

	if( o.ldap ) o = o.Ldap( password );
	if( o.plain ) o = o.Plain( password );
	o = o.Shadow( password );

	return o;
};
