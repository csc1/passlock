/*
| A plain password.
*/
def.attributes =
{
	// the digest
	password: { type: 'string', json: true },
};

def.json = true;

/*
| Checks a password.
|
| ~return true: password match
| ~return false: password mismatch
*/
def.proto.checkPassword =
	function( password )
{
	return password === this.password;
};

/*
| Creates a passhash from password.
| Salt is autogenerated.
*/
def.static.Password =
	function( password )
{
	return Self.create( 'password', password );
};
