/*
| The data about a ssh key
*/
def.extend = 'list@SshKey/Self';

def.json = true;

import { Self as SSHKeyData } from '{SshKey/Self}';

/*
| Returns the sshkey list as text.
*/
def.lazy.asText =
	function( )
{
	let text = '';
	for( let entry of this )
	{
		text += entry.asText + '\n';
	}

	return text;
};

/*
| Creates the sshkey data from text of line strings.
*/
def.static.FromText =
	function( text )
{
	const lines = text.split( '\n' );
	const list = [ ];

	for( let line of lines )
	{
		line = line.trim( );
		if( line === '' )
		{
			continue;
		}

		const d = SSHKeyData.FromLine( line );
		if( d )
		{
			list.push( d );
		}
	}

	return Self.Array( list );
};
