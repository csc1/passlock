/*
| The data about a ssh key
*/
def.attributes =
{
	// the algorithm
	algorithm: { type: 'string', json: true },

	// the key name ("comment")
	name: { type: 'string', json: true },

	// the key
	key: { type: 'string', json: true },
};

def.json = true;

//import ssh2 from 'ssh2';

/*
| Returns the sshkey list as text.
*/
def.lazy.asText =
	function( )
{
	return this.algorithm + ' ' + this.key + ' ' + this.name;
};

/*
| Creates the sshkey data from a line string.
| Must already be have trimmed.
*/
def.static.FromLine =
	function( line )
{
	let ios = line.indexOf( ' ' );
	if( ios < 0 )
	{
		return;
	}
	const algorithm = line.substr( 0, ios );
	line = line.substr( ios + 1 );
	ios = line.indexOf( ' ' );
	const key = line.substr( 0, ios );
	let name = line.substr( ios + 1 );
	name = name.trim( );
	return Self.create( 'algorithm', algorithm, 'key', key, 'name', name );
};
